@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card bg-light mt-3">
            <div class="card-header">
                Telephone Directory
            </div>

            <div class="card-body">
                <form action="{{ route('telephone-directories.import') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="file" class="form-control">
                    <br>
                    <button class="btn btn-success">Import Telephone-directories Data</button>
                </form>

                <table class="table table-bordered mt-3">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Telephone</th>
                    </tr>

                    @foreach($telephoneDirectories as $telephoneDirectory)
                        <tr>
                            <td>{{ $telephoneDirectory->telephone_directory_id }}</td>
                            <td>{{ $telephoneDirectory->name }}</td>
                            <td>{{ $telephoneDirectory->telephone }}</td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
@endsection
