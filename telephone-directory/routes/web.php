<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TelephoneDirectoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::controller(TelephoneDirectoryController::class)->group(function(){
    Route::get('/home', 'index')->name('home');
    Route::post('telephone-directories-import', 'import')->name('telephone-directories.import');

});