<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelephoneDirectory extends Model
{
    use HasFactory;



    protected $table = 'telephone_directories';
    protected $primaryKey = 'telephone_directory_id';
// Disable the model timestamps
    public $timestamps = false;

    protected $fillable = [
        'name',
        'telephone',
    ];
}
