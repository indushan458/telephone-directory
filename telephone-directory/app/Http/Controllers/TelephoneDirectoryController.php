<?php

namespace App\Http\Controllers;

use App\Imports\TelephoneDirectoryImport;
use App\Models\TelephoneDirectory;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TelephoneDirectoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $telephoneDirectories = TelephoneDirectory::get();
        return view('telephoneDirectories', compact('telephoneDirectories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(TelephoneDirectory $telephoneDirectory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TelephoneDirectory $telephoneDirectory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, TelephoneDirectory $telephoneDirectory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TelephoneDirectory $telephoneDirectory)
    {
        //
    }


    /**

     * @return \Illuminate\Support\Collection

     */

    public function import()
    {

//        dd(request()->file('file'));
        Excel::import(new TelephoneDirectoryImport(),request()->file('file'));
        return back();
    }
}
