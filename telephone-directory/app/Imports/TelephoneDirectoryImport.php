<?php

namespace App\Imports;

use App\Models\TelephoneDirectory;
use Maatwebsite\Excel\Concerns\ToModel;

class TelephoneDirectoryImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new TelephoneDirectory([

            'name'     => $row[0],
            'telephone'    => $row[1],
        ]);
    }
}
